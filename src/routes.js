import Buttons from "views/components/Buttons.jsx";
import Calendar from "views/Calendar.jsx";
import Charts from "views/Charts.jsx";
import Dashboard from "views/Dashboard.jsx";
import ExtendedTables from "views/tables/ExtendedTables.jsx";
import AdvertiseManager from "views/tables/AdvertiseManager.jsx";
import LicenseManager from "views/tables/LicenseManager.jsx";
import GoogleMaps from "views/maps/GoogleMaps.jsx";
import GridSystem from "views/components/GridSystem.jsx";
import Icons from "views/components/Icons.jsx";
import Notifications from "views/components/Notifications.jsx";
import Panels from "views/components/Panels.jsx";
import ReactTables from "views/tables/ReactTables.jsx";
import RegularTables from "views/tables/RegularTables.jsx";
import SweetAlert from "views/components/SweetAlert.jsx";
import Typography from "views/components/Typography.jsx";
import VectorMap from "views/maps/VectorMap.jsx";
import Widgets from "views/Widgets.jsx";
import Wizard from "views/forms/Wizard.jsx";
import ChangeInfo from "views/ChangeInfo/ChangeInfo.jsx";
import ListUserManager from "views/Managers/UsersManager/ListUserManager";
import CreateUserManager from "views/Managers/UsersManager/CreateUserManager";
import HotSpotList from "views/Hotspot/HotSpotList.jsx";
import CreateHotSpot from "views/Hotspot/CreateHotSpot.jsx";
import Login from "views/Authentication/SignIn";
import GiangVien from "views/GiangVien/GiangVien.jsx";

const routes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: "nc-icon nc-bank",
    component: Dashboard,
    layout: "/admin"
  },
  {
    path: "/user-profile",
    name: "UserProfile",
    mini: "UP",
    component: ChangeInfo,
    layout: "/admin",
    invisible: true
  },
  {
    path: "/hotspotlist",
    icon: "fas fa-user-graduate",
    name: "Sinh Viên",
    mini: "H",
    component: HotSpotList,
    layout: "/admin"

  },
  {
    path: "/giangvien",
    icon: "fas fa-chalkboard-teacher",
    name: "Giảng Viên",
    mini: "H",
    component: GiangVien,
    layout: "/admin"

  },
  {
    path: "/createhotspot",
    icon: "fas fa-wifi",
    name: "ADD HOTSPOT",
    mini: "H",
    component:  CreateHotSpot,
    layout: "/admin",
    invisible: true

  },
  {
    path: "/login",
    name: "Setting",
    //mini: "S",
    component: Login,
    layout: "/auth",
    invisible: true
  },
  {
    path: "/wizard",
    name: "Create Advertise",
    icon: "nc-icon nc-ruler-pencil",
    component: Wizard,
    layout: "/admin",
    invisible: true
  },
  // {
  //   collapse: true,
  //   name: "Forms",
  //   icon: "nc-icon nc-ruler-pencil",
  //   state: "formsCollapse",
  //   views: [
  //     {
  //       path: "/regular-forms",
  //       name: "Regular Forms",
  //       mini: "RF",
  //       component: RegularForms,
  //       layout: "/admin",
  //       invisible: true
  //     },
  //     {
  //       path: "/extended-forms",
  //       name: "Extended Forms",
  //       mini: "EF",
  //       component: ExtendedForms,
  //       layout: "/admin",
  //       invisible: true
  //     },
  //     {
  //       path: "/validation-forms",
  //       name: "Validation Forms",
  //       mini: "VF",
  //       component: ValidationForms,
  //       layout: "/admin",
  //       invisible: true
  //     },
  //     //
  //   ]
  // },
  // {
  //   collapse: true,
  //   name: "Managers",
  //   icon: "nc-icon nc-single-copy-04",
  //   state: "tablesCollapse",
  //   invisible: true,
  //   views: [
  //     {
  //       path: "/regular-tables",
  //       name: "Regular Tables",
  //       mini: "RT",
  //       component: RegularTables,
  //       layout: "/admin",
  //       invisible: true
  //     },
  //     {
  //       path: "/extended-tables",
  //       name: "Extended Tables",
  //       mini: "ET",
  //       component: ExtendedTables,
  //       layout: "/admin",
  //       invisible: true
  //     },
  //     {
  //       path: "/react-tables",
  //       name: "React Tables",
  //       mini: "RT",
  //       component: ReactTables,
  //       layout: "/admin",
  //       invisible: true
  //     },
  //     {
  //       path: "/advertise-manager",
  //       name: "Advertise Manager",
  //       mini: "AM",
  //       component: AdvertiseManager,
  //       layout: "/admin"
  //     },
  //     {
  //       path: "/users-manager",
  //       name: "Users Manager",
  //       mini: "UM",
  //       component: ListUserManager,
  //       layout: "/admin"
  //     },
  //     {
  //       path: "/create-users-manager",
  //       name: "Create Users Manager",
  //       mini: "UM",
  //       component: CreateUserManager,
  //       layout: "/admin",
  //       invisible: true
  //     }
  //     ,
  //     {
  //       path: "/license-manager",
  //       name: "License Manager",
  //       mini: "LM",
  //       component: LicenseManager,
  //       layout: "/admin"
  //     }
  //   ]
  // },
  // {
  //   collapse: true,
  //   name: "Location Managers",
  //   icon: "nc-icon nc-pin-3",
  //   state: "mapsCollapse",
  //   views: [
  //     {
  //       path: "/google-maps",
  //       name: "Google Maps",
  //       mini: "GM",
  //       component: GoogleMaps,
  //       layout: "/admin"
  //     },
  //     // {
  //     //   path: "/full-screen-map",
  //     //   name: "Full Screen Map",
  //     //   mini: "FSM",
  //     //   component: FullScreenMap,
  //     //   layout: "/admin"
  //     // },
  //     {
  //       path: "/vector-map",
  //       name: "Vector Map",
  //       mini: "VM",
  //       component: VectorMap,
  //       layout: "/admin",
  //       invisible: true
  //     }
  //   ]
  // },
  //Example of Paper Admin pro ------------------------------------------------------------------------
  // {
  //   collapse: true,
  //   name: "Components",
  //   icon: "nc-icon nc-layout-11",
  //   state: "componentsCollapse",
  //   invisible: true,
  //   views: [
  //     {
  //       path: "/buttons",
  //       name: "Buttons",
  //       mini: "B",
  //       component: Buttons,
  //       layout: "/admin",
  //       invisible: true
  //     },
  //     {
  //       path: "/grid-system",
  //       name: "Grid System",
  //       mini: "GS",
  //       component: GridSystem,
  //       layout: "/admin",
  //       invisible: true
  //     },
  //     {
  //       path: "/panels",
  //       name: "Panels",
  //       mini: "P",
  //       component: Panels,
  //       layout: "/admin",
  //       invisible: true
  //     },
  //     {
  //       path: "/sweet-alert",
  //       name: "Sweet Alert",
  //       mini: "SA",
  //       component: SweetAlert,
  //       layout: "/admin",
  //       invisible: true
  //     },
  //     {
  //       path: "/notifications",
  //       name: "Notifications",
  //       mini: "N",
  //       component: Notifications,
  //       layout: "/admin",
  //       invisible: true
  //     },
  //     {
  //       path: "/icons",
  //       name: "Icons",
  //       mini: "I",
  //       component: Icons,
  //       layout: "/admin",
  //       invisible: true
  //     },
  //     {
  //       path: "/typography",
  //       name: "Typography",
  //       mini: "T",
  //       component: Typography,
  //       layout: "/admin",
  //       invisible: true
  //     }
  //   ]
  // },
  {
    path: "/widgets",
    name: "Widgets",
    icon: "nc-icon nc-box",
    component: Widgets,
    layout: "/admin",
    invisible: true
  },
  {
    path: "/charts",
    name: "Charts",
    icon: "nc-icon nc-chart-bar-32",
    component: Charts,
    layout: "/admin",
    invisible: true
  },
  {
    path: "/calendar",
    name: "Lịch giảng daỵ",
    icon: "nc-icon nc-calendar-60",
    component: Calendar,
    layout: "/admin",
  }
];

export default routes;
