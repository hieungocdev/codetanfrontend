import React, { Component } from 'react';
import CreateAdmin from "../../Api/AdminManage/CreateAdmin";
  import NotificationAlert from "react-notification-alert";

// reactstrap components
import {
     Input,Button, Card, InputGroup, InputGroupAddon, InputGroupText, CardBody

  } from "reactstrap";
class LicenseModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name:"",  
      email:"",
      username: "",
      password: "",
      nameState:"",
      emailState:"",
      usernameState: "",
      passwordState: "",
      alert_message: "",
      visible: true
    };
    this.onChange = this.onChange.bind(this);
  }
  verifyNumber = value => {
    var numberRex = new RegExp("^[0-9]+$");
    if (numberRex.test(value)) {
      return true;
    }
    return false;
  };
    // hàm kiểm tra emial
    verifyEmail = value => {
      var emailRex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if (emailRex.test(value)) {
        return true;
      }
      return false;
    };
    // hàm kiem tra doi dai
    verifyLength = (value, length) => {
      if (value.length >= length) {
        return true;
      }
      return false;
    };
     // hàm lấy các giá trị bàn phím
     onChange(e) {
      this.setState({ [e.target.name]: e.target.value });
    }
  // thong tao tai khoan bi loi
  onFail = place => {  
    var options = {};
    options = {
      place: place,
      message: (
        <div>
          <div>
             <b>
             Add account failed, please check again

             </b>

          </div>
        </div>
      ),
      type: 'danger',
      icon: "now-ui-icons ui-1_bell-53",
      autoDismiss: 7
    };
    this.refs.notificationAlert.notificationAlert(options);
  };

  // thong bao email đã tồn tại
  onFailEmail = place => {  
    var options = {};
    options = {
      place: place,
      message: (
        <div>
          <div>
             <b>
             Email already exists, please try again

             </b>

          </div>
        </div>
      ),
      type: 'danger',
      icon: "now-ui-icons ui-1_bell-53",
      autoDismiss: 7
    };
    this.refs.notificationAlert.notificationAlert(options);
  };
 // thong bao username đã tồn tại
 onFailUserName = place => {  
  var options = {};
  options = {
    place: place,
    message: (
      <div>
        <div>
           <b>
          Username already exists, please try again

           </b>

        </div>
      </div>
    ),
    type: 'danger',
    icon: "now-ui-icons ui-1_bell-53",
    autoDismiss: 7
  };
  this.refs.notificationAlert.notificationAlert(options);
};

  Create = () => {
    const { name, email, username, password } = this.state;

    if (this.state.nameState === "") {
      this.setState({ nameState: "has-danger" });
    }
    if (this.state.emailState === "") {
      this.setState({ emailState: "has-danger" });
    }
    if (this.state.usernameState === "") {
      this.setState({ usernameState: "has-danger" });
    }
    if (this.state.passwordState === "") {
      this.setState({ passwordState: "has-danger" });
    }
    CreateAdmin(name, email, username, password)
    .then(res =>{
           if(res.message === 'EMAIL_IS_ALREADY_EXISTS'){
                  this.onFailEmail("tr");        
           }  if(res.message === 'USERNAME_IS_ALREADY_EXISTS'){
            this.onFailUserName("tr");        
     } 
           else if(res.expressIn === 201){
                  this.setState(
                         //this.props.history.push("/admin/users-manager")
             //<Redirect  to={{pathname:'/admin/users-manager'}} />
                         );
                         
           }
           console.log(res)
    })
  }





  change = (event, stateName, type, stateNameEqualTo, maxValue) => {
    switch (type) {
      case "email":
        if (this.verifyEmail(event.target.value)) {
          this.setState({ [stateName + "State"]: "has-success" });
        } else {
          this.setState({ [stateName + "State"]: "has-danger" });
        }
        break;
      case "password":
        if (this.verifyLength(event.target.value, 1)) {
          this.setState({ [stateName + "State"]: "has-success" });
        } else {
          this.setState({ [stateName + "State"]: "has-danger" });
        }
        break;
      case "equalTo":
        if (this.compare(event.target.value, this.state[stateNameEqualTo])) {
          this.setState({ [stateName + "State"]: "has-success" });
          this.setState({ [stateNameEqualTo + "State"]: "has-success" });
        } else {
          this.setState({ [stateName + "State"]: "has-danger" });
          this.setState({ [stateNameEqualTo + "State"]: "has-danger" });
        }
        break;
      case "number":
        if (this.verifyNumber(event.target.value)) {
          this.setState({ [stateName + "State"]: "has-success" });
        } else {
          this.setState({ [stateName + "State"]: "has-danger" });
        }
        break;
      case "length":
        if (this.verifyLength(event.target.value, stateNameEqualTo)) {
          this.setState({ [stateName + "State"]: "has-success" });
        } else {
          this.setState({ [stateName + "State"]: "has-danger" });
        }
        break;
      case "max-length":
        if (!this.verifyLength(event.target.value, stateNameEqualTo + 1)) {
          this.setState({ [stateName + "State"]: "has-success" });
        } else {
          this.setState({ [stateName + "State"]: "has-danger" });
        }
        break;
      case "url":
        if (this.verifyUrl(event.target.value)) {
          this.setState({ [stateName + "State"]: "has-success" });
        } else {
          this.setState({ [stateName + "State"]: "has-danger" });
        }
        break;
      case "min-value":
        if (
          this.verifyNumber(event.target.value) &&
          event.target.value >= stateNameEqualTo
        ) {
          this.setState({ [stateName + "State"]: "has-success" });
        } else {
          this.setState({ [stateName + "State"]: "has-danger" });
        }
        break;
      case "max-value":
        if (
          this.verifyNumber(event.target.value) &&
          event.target.value <= stateNameEqualTo
        ) {
          this.setState({ [stateName + "State"]: "has-success" });
        } else {
          this.setState({ [stateName + "State"]: "has-danger" });
        }
        break;
      default:
        break;
    }
    this.setState({ [stateName]: event.target.value });
  };



    componentWillReceiveProps(nextProps) {
        this.setState({
          packagename: nextProps.packagename,
          image: nextProps.image,
          adsposted: nextProps.adsposted,
          view: nextProps.view,
          expiry: nextProps.expiry,
          price: nextProps.price,
          status: nextProps.status
        });
    }

    packagenameHandler(e) {
        this.setState({ packagename: e.target.value });
    }

    imageHandler(e) {
        this.setState({ image: e.target.value });
    }

    adspostedHandler(e) {
        this.setState({ adsposted: e.target.value });
    }

    viewHandler(e) {
        this.setState({ view: e.target.value });
    }

    expiryHandler(e) {
        this.setState({ expiry: e.target.value });
    }

    priceHandler(e) {
        this.setState({ price: e.target.value });
    }

    handleEdit() {
        this.setState({ status: false });
        const item = this.state;
        this.props.editModalDetails(item)
    }

    handleAdd() {
        this.setState({
            status : false
        });
        const item = this.state;
        this.props.addModalDetails(item)
    }

    render() {
      let {
      nameState,
      emailState,
      usernameState,
      passwordState,
      minLengthState,
      maxLengthState,
      } = this.state;
        return (
          <div className="modal fade" id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" >
            <NotificationAlert ref="notificationAlert" />

            <div className="modal-dialog modal-lg" role="document">
              <div className="modal-content">
                <Card className=" card-login card-plain">
                  <div className=" modal-header justify-content-center">
                    <button
                      aria-label="Close"
                      className=" close"
                      data-dismiss="modal"
                      type="button"
                      onClick={this.toggleModalLogin}
                    >
                      <span aria-hidden={true}>×</span>
                    </button>
                    <div className=" header header-primary text-center">
                      <div className=" logo-container">
                        <img
                          alt="..."
                          src={require("assets/img/logo-small.png")}
                          style={{ width: "20%" }}
                        />
                      </div>
                    </div>
                  </div>
                  <div className=" modal-body">
                    <div>

                      <CardBody>
                        <InputGroup className={`has-label ${nameState}`}>
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className=" nc-icon nc-single-02" />
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            placeholder="Full name"
                            name="name"
                            type="text"

                            onChange={e => this.change(e, "name", "length", 1)}
                          />
                          {this.state.nameState === "has-danger" ? (
                            <label className="error">This field is required.</label>
                          ) : null}
                        </InputGroup>

                        <InputGroup className={`has-label ${emailState}`}>
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className=" nc-icon nc-email-85" />
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input

                            placeholder="Email"
                            name="email"
                            type="email"
                            required
                            onChange={e => this.change(e, "email", "email")}
                          />
                          {this.state.emailState === "has-danger" ? (
                            <label className="error">
                              Please enter a valid email address.
                        </label>
                          ) : null}
                        </InputGroup>

                        <InputGroup className={`has-label ${usernameState}`}>
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className=" nc-icon nc-single-02" />
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            placeholder="Username"
                            name="username"
                            type="text"

                            onChange={e => this.change(e, "username", "length", 1)}
                          />
                          {this.state.usernameState === "has-danger" ? (
                            <label className="error">This field is required.</label>
                          ) : null}
                        </InputGroup>

                        <InputGroup className={`has-label ${passwordState}`}>
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className=" nc-icon nc-key-25" />
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            placeholder="Password"
                            name="password"
                            type="password"
                            autoComplete="off"

                            onChange={e =>
                              this.change(e, "password", "max-length", 30)
                            }
                          />
                          {this.state.passwordState === "has-danger" ? (
                            <label className="error">Password length of 6-30 characters. Please try again</label>
                          ) : null}
                        </InputGroup>


                      </CardBody>
                    </div>
                  </div>
                  <div className=" modal-footer text-center">
                    <Button

                      block
                      className=" btn-neutral btn-round"
                      color=""
                      onClick={this.Create}
                    >
                      Get Started
                  </Button>
                  </div>
                </Card>
              </div>
            </div>
          </div>
        );
    }
}

export default LicenseModal;
