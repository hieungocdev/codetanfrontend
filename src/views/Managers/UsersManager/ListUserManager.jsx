import React from "react";
import Modal from '../../modal/LicenseModal.jsx';
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  Table,
  Row,
  Col,
  UncontrolledTooltip
} from "reactstrap";
// import api ListAdmin
import ListAdmin from '../../../Api/AdminManage/ListAdmin';
class ListUserManager extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      requiredItem: 0,
      brochure: [ ]
    }
  }
  componentDidMount (){
    ListAdmin()
   .then((resJson)=>{
     this.setState({
      brochure:resJson
     })
   })
    
    .catch(err => console.log(err));
  }
  // to stop the warning of calling setState of unmounted component
  componentWillUnmount() {
    var id = window.setTimeout(null, 0);
    while (id--) {
      window.clearTimeout(id);
    }
  }
  replaceModalItem = (modalID) => {
    this.setState({
      requiredItem: modalID
    });
  }
  addModalItem () {
                this.props.history.push("/admin/create-users-manager")

  }

  editModalDetails = (item) => {
    const requiredItem = this.state.requiredItem;
    let tempbrochure = this.state.brochure;
    tempbrochure[requiredItem] = item;

    this.setState({ brochure: tempbrochure });
  }
  addModalDetails = (item) => {
    console.log('try');

        let tempbrochure = this.state.brochure;
        //alert(this.state.brochure.length);
        let datas = item;

        let packagename = datas.packagename;
        let image = datas.image;
        let adsposted = datas.adsposted;
        let view = datas.view;
        let expiry = datas.expiry;
        let price = datas.price;
        let status = 'false';
        if(this.state.brochure.length === 0){   //new
        let data = {
            packagename, image , adsposted, view, expiry, price, status
        }
        tempbrochure.push(data);
        }else{                      //update
            let countBrochure = this.state.brochure.length;
            tempbrochure[countBrochure] = item;
            this.setState({ brochure: tempbrochure });
        }
  }

  deleteItem(modalID) {
    let tempBrochure = this.state.brochure;
    tempBrochure.splice(modalID, 1);
    this.setState({ brochure: tempBrochure });
  }
  hideItem(modalID) {
    let tempbrochure = this.state.brochure;
    if(tempbrochure[modalID].status === 'true'){
      tempbrochure[modalID].status = 'false';
    }else {
      tempbrochure[modalID].status = 'true';
    }
    this.setState({ brochure: tempbrochure });
  }

  render() {
    const brochure = this.state.brochure.map((item, modalID) => {
    return (
        <tr key="modalID">
          <td className="text-center">{modalID+1}</td>
          <td>{item.name}</td>
          <td>{item.email}</td>
          <td className="text-center">{item.type}</td>
          <td className="text-center">{item.level}</td>
          <td className="text-center">{item.status}</td>
          <td className="text-center" >

              <Button
                className="btn-icon col-md-4"
                color="danger"
                id="tooltip2"
                size="sm"
                type="button"
                onClick={() => this.hideItem(modalID)}
              >
                <i className="fa fa-key" />
              </Button>{" "}
              <UncontrolledTooltip
                delay={0}
                target="tooltip2"
              >
                Lock
              </UncontrolledTooltip>

              <Button
                className="btn-icon col-md-4"
                color="success"
                id="tooltip366246651"
                size="sm"
                type="button"
                data-toggle="modal" data-target="#exampleModal"
                onClick={() => this.replaceModalItem(modalID)}
              >
                <i className="fa fa-edit" />
              </Button>{" "}
              <UncontrolledTooltip
                delay={0}
                target="tooltip366246651"
              >
                Edit
              </UncontrolledTooltip>

                <Button
                  className="btn-icon col-md-4"
                  color="danger"
                  id="tooltip476609793"
                  size="sm"
                  type="button"
                  onClick={() => this.deleteItem(modalID)}
                >
                  <i className="fa fa-times" />
                </Button>{" "}
                <UncontrolledTooltip
                  delay={0}
                  target="tooltip476609793"
                >
                  Delete
                </UncontrolledTooltip>

          </td>
        </tr>

    )
  });
    const requiredItem = this.state.requiredItem;
    let modalData = this.state.brochure[requiredItem];
    return (
      <>
        <div className="content">
          <Row>
            <Col md="12">
              <Card>
                <CardHeader>
                    <h4 className="card-title float-left ml-4">User Manager</h4>
                    <div className= "float-right mr-4">
                      <Button
                            className="btn-icon "
                            color="info"
                            id="tooltip264453217"
                            size="md"
                            type="button"
                            data-toggle="modal" data-target="#exampleModal"
                            onClick={() => this.addModalItem()}
                          >
                            <i className="fas fa-plus rounded" />
                          </Button>{" "}
                          <UncontrolledTooltip
                            delay={0}
                            target="tooltip264453217"
                          >
                            Add
                          </UncontrolledTooltip>
                    </div>
                </CardHeader>
                <CardBody>
                  <Table responsive striped>
                    <thead className="text-primary">
                      <tr>
                        <th className="text-center">#</th>
                        <th>Name</th>
                        <th className="text-center">Email</th>
                        <th className="text-center">Group</th>
                        <th className="text-center">Level</th>
                        <th className="text-right">Status</th>         
                        <th className="text-right">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      {brochure}
                    </tbody>
                  </Table>

                </CardBody>
              </Card>
            </Col>
          </Row>
          { this.state.brochure.length !== 0 ?
          <Modal
            packagename={modalData.packagename}
            image={modalData.image}
            adsposted={modalData.adsposted}
            view={modalData.view}
            expiry={modalData.expiry}
            price={modalData.price}
            editModalDetails={this.editModalDetails}
            addModalDetails={this.addModalDetails}
          /> :
          <Modal
            packagename=''
            image=''
            adsposted=''
            view=''
            expiry=''
            price= ''
            status =  'false'
            editModalDetails={this.editModalDetails}
            addModalDetails={this.addModalDetails}
          />
          }
        </div>

      </>
    );
  }
}


export default ListUserManager;
