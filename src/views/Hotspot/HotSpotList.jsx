import React from "react";
// react component for creating dynamic tables
import ReactTable from "react-table";

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Row,
  Col
} from "reactstrap";

const dataTable = [
  ["1511060629","Lê Nguyễn Minh Tân", "08/07/1997","15dth04","2015-2019"],
  ["1511060629","Lê Nguyễn Minh Tân", "08/07/1997","15dth04","2015-2019"],
  ["1511060629","Lê Nguyễn Minh Tân", "08/07/1997","15dth04","2015-2019"],
  ["1511060629","Lê Nguyễn Minh Tân", "08/07/1997","15dth04","2015-2019"],
  ["1511060629","Lê Nguyễn Minh Tân", "30/07/1997","15dth04","2015-2019"],
  ["1511060629","Lê Nguyễn Minh Tân", "13/07/1997","15dth04","2015-2019"],
  ["1511060629","Lê Nguyễn Minh Tân", "22/07/1997","15dth04","2015-2019"],
  ["1511060629","Lê Nguyễn Minh Tân", "11/07/1997","15dth04","2015-2019"],
  ["1511060629","Lê Nguyễn Minh Tân", "10/07/1997","15dth04","2015-2019"]
];

export default class HotSpotList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

      data: dataTable.map((prop, key) => {
        return {
          id: key,
          ID: prop[0],
          tenHotspot: prop[1],
          diaChi: prop[2],
          dangTruyCap: prop[3],
          luotXem: prop[4],
          luotTruyCap: prop[5],
          trangThai: prop[6],
          actions: (
            // we've added some custom button actions
            <div className="actions-right">
              {/* cấu hình hotspot */}
              <Button

              color="black"
                size="sm"
                className="btn-icon btn-link like"
              >
                <i className="fas fa-cog" />
              </Button>{" "}
              {/* chỉnh sửa hotspot */}
              <Button

              color="primary"
                size="sm"
                className="btn-icon btn-link like"
              >
                <i className="fas fa-pencil-alt" />
              </Button>{" "}
              {/* use this button to add a edit kind of action */}

              {/* xoa hotspot */}
              <Button
                onClick={() => {
                  var data = this.state.data;
                  data.find((o, i) => {
                    if (o.id === key) {
                      // here you should add some custom code so you can delete the data
                      // from this component and from your server as well
                      data.splice(i, 1);
                      console.log(data);
                      return true;
                    }
                    return false;
                  });
                  this.setState({ data: data });
                }}
                color="danger"
                size="sm"
                className="btn-icon btn-link remove"
              >
                <i className="fa fa-times" />
              </Button>{" "}
            </div>
          )
        };
      })
    };
  }
  CreateHotSpot = () => {
    this.props.history.push('/admin/createhotspot');

  };
  render() {
    return (
      <>
        <div className="content">
          <Row>
            <Col md="12">
              <Card>
                <CardHeader>
                  <CardTitle tag="h4" className="float-left ml-2">Danh sách sinh viên</CardTitle>
                  <Button
                    onClick={this.CreateHotSpot}
                    color="primary" size="Normal" className="float-right mr-2">Thêm mới</Button>
                </CardHeader>
                <div className="button">
                </div>
                <CardBody>
                  <ReactTable
                    data={this.state.data}
                    filterable
                    columns={[
                      {
                        Header: "MSSV",
                        accessor: "ID",
                        maxWidth:60
                      },
                      {
                        Header: "Tên sinh viên",
                        accessor: "tenHotspot",
                        maxWidth:500

                      },
                      {
                        Header: "Ngày sinh",
                        accessor: "diaChi",
                        maxWidth:1000

                      },
                      {
                        Header: "Lớp",
                        accessor: "dangTruyCap",
                        maxWidth:100

                      },
                      {
                        Header: "Niên khoá",
                        accessor: "luotXem",
                        maxWidth:100
                      },
                    
                  

                      {
                        Header: "Tác vụ",
                        accessor: "actions",
                        sortable: false,
                        filterable: false,
                        maxWidth:150
                      }
                    ]}
                    defaultPageSize={10}
                    showPaginationTop
                    showPaginationBottom={false}
                    /*
                      You can choose between primary-pagination, info-pagination, success-pagination, warning-pagination, danger-pagination or none - which will make the pagination buttons gray
                    */
                    className="-striped -highlight primary-pagination"
                  />
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}
