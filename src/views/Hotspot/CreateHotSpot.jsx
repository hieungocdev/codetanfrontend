import React from 'react';
import Select from "react-select";

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Row,
  Col,
  Form,
  Input,
  FormGroup
} from "reactstrap";
export default class CreateHotSpot extends React.Component {
  constructor() {
    super();
    this.state = {
      
      zoom: 13,
  maptype: 'roadmap',
  place_formatted: '',
  place_id: '',
  place_location: '',
    }
  }
  componentDidMount() {
    let map = new window.google.maps.Map(document.getElementById('map'), {
      zoom: 13,
      mapTypeId: 'roadmap',
    });
    map.addListener('zoom_changed', () => {
      this.setState({
        zoom: map.getZoom(),
      });
    });
    
    map.addListener('maptypeid_changed', () => {
      this.setState({
        maptype: map.getMapTypeId(),
      });
    });
    let marker = new window.google.maps.Marker({
      map: map,
      position: {lat: -33.8688, lng: 151.2195},
    });
    
    // initialize the autocomplete functionality using the #pac-input input box
    let inputNode = document.getElementById('pac-input');
    let autoComplete = new window.google.maps.places.Autocomplete(inputNode);
    
    autoComplete.addListener('place_changed', () => {
      let place = autoComplete.getPlace();
      let location = place.geometry.location;
    
      this.setState({
        //place_formatted: place.formatted_address,
        place_id: place.place_id,
        place_location: location.toString(),
      });
    
      // bring the selected place in view on the map
      map.fitBounds(place.geometry.viewport);
      map.setCenter(location);
    
      marker.setPlace({
        placeId: place.place_id,
        location: location,
      });
    });
  }

  render() {
    return (
           
      <>
      
        <div className="content">
          <Row>
            <Col md="12">
              <Card>
                <CardHeader>
                  <CardTitle tag="h4">Thêm HotSpot</CardTitle>
                </CardHeader>
                <CardBody>
                <Form action="#" method="#">
                <Row>
                <Col md="6">
                <label>Tên HotSpot</label>
                    <FormGroup>
                      <Input placeholder="Nhập tên HotSpot" type="text" />
                    </FormGroup>      
                    <label>Select your hardware</label>
                    <FormGroup className="from-group"> 
                    <Select
                            className="react-select primary"
                            classNamePrefix="react-select"
                            name="singleSelect"
                            value={this.state.singleSelect}
                            onChange={value =>
                              this.setState({ singleSelect: value })
                            }
                            options={[
                              {
                                value: "",
                                label: "Select hardware",
                                isDisabled: true
                              },
                              { value: "2", label: "4ipnet-60ndc" },
                              { value: "3", label: "4ipnet-9nk" }
                            ]}
                            placeholder="Select hardware"
                          />
                    </FormGroup>   
                    <label>Lincense</label>
                    <FormGroup>
                      <Input placeholder="Nhập Lincense" type="text" />
                    </FormGroup>  
                    <label>Thời gian của một phiên giao dịch</label>
                    <FormGroup>
                      <Input placeholder="Nhập thời gian của một phiên giao dịch" type="text" />
                    </FormGroup>
                    <label>Băng thông download của một khách hàng</label>
                    <FormGroup>
                    <Select
                            className="react-select primary"
                            classNamePrefix="react-select"
                            name="singleSelect"
                            value={this.state.singleSelect}
                            onChange={value =>
                              this.setState({ singleSelect: value })
                            }
                            options={[
                              {
                                value: "",
                                label: "Select bw",
                                isDisabled: true
                              },
                              { value: "2", label: "100mb" },
                              { value: "3", label: "200mb" }
                            ]}
                            placeholder="Select bw"
                          />
                    </FormGroup>
                    <label>Băng thông upload của một khách hàng</label>
                    <FormGroup>
                    <Select
                            className="react-select primary"
                            classNamePrefix="react-select"
                            name="singleSelect"
                            value={this.state.singleSelect}
                            onChange={value =>
                              this.setState({ singleSelect: value })
                            }
                            options={[
                              {
                                value: "",
                                label: "Select bw",
                                isDisabled: true
                              },
                              { value: "2", label: "100mb" },
                              { value: "3", label: "200mb" }
                            ]}
                            placeholder="Select bw"
                          />
                    </FormGroup>
                    <label>ssid</label>
                    <FormGroup>
                      <Input placeholder="Nhập SSID " type="text" />
                    </FormGroup>
                    <label>Chọn quảng cáo sẽ chạy trên thiết bị này</label>
                    <FormGroup>
                    <Select
                            className="react-select info"
                            classNamePrefix="react-select"
                            placeholder="Chọn mô hình quảng cáo"
                            name="quangcao"
                            closeMenuOnSelect={false}
                            isMulti
                            value={this.state.quangcao}
                            onChange={value =>
                              this.setState({ quangcao: value })
                            }
                            options={[
                              {
                                value: "",
                                label: " Danh sách quảng cáo",
                                isDisabled: true
                              },
                              { value: "6", label: "Banner" },
                              { value: "7", label: "Video" },
                              { value: "8", label: "Khảo sát" },
                          
                            ]}
                          />
                    </FormGroup>
                </Col>
                <Col md="6">
                <label>Địa chỉ</label>
                <Input id='pac-input' type='text' placeholder='Enter a location' />
                    <FormGroup>
                    
                    <div id="map" style={{width:"100%", height:250}}></div>
                    </FormGroup>

                    <label>Loại địa điểm</label>
                    <FormGroup>
                    <Select
                            className="react-select info"
                            classNamePrefix="react-select"
                            placeholder="Chọn địa điểm chạy quảng cáo"
                            name="multipleSelect"
                            closeMenuOnSelect={false}
                            isMulti
                            value={this.state.multipleSelect}
                            onChange={value =>
                              this.setState({ multipleSelect: value })
                            }
                            options={[
                              {
                                value: "",
                                label: " Danh sách địa điểm",
                                isDisabled: true
                              },
                              { value: "2", label: "Phố đi bộ" },
                              { value: "3", label: "Trường học" },
                              { value: "4", label: "Bệnh viện" },
                          
                            ]}
                          />                    
                    <label>Thông tin thêm</label>
                    <FormGroup>
                    <Input id="pac-input"  type="text" placeholder="Nhập địa chỉ" />
                    
                    </FormGroup>
                    </FormGroup>
                </Col>
                </Row>
                <div className="button">

                <button class="btn btn-primary btn-lg">Save</button>
                </div>
            </Form>
                </CardBody>
                
        </Card>
        </Col>
        </Row>
        </div>
      </>
    );
  }
}
