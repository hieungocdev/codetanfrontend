import React from "react";
import "../../../node_modules/video-react/dist/video-react.css";
import { Player, ControlBar, PlayToggle,CurrentTimeDisplay, TimeDivider ,PlaybackRateMenuButton} from "video-react";
// reactstrap components
import {
  Container,
  Button

} from "reactstrap";

const sources = {
  bunnyTrailer: 'http://125.212.241.80/video.mp4',
  bunnyMovie: 'http://125.212.241.80/video.mp4',
};

class LockScreen extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      source: sources['bunnyMovie'],
      "duration": 596.48,
      "currentTime": 0,
      "seekingTime": 0,
      "buffered": {},
      "waiting": false,
      "seeking": false,
      "paused": true,
      "ended": false,
      "playbackRate": 1,
      "muted": false,
      "volume": 1,
      "readyState": 4,
      "networkState": 2,
      "videoWidth": 853,
      "videoHeight": 480,
      "hasStarted": false,
      "userActivity": false,
      "isActive": true,
      "isFullscreen": true,
      "error": null,
      "src": "",
      "srcObject": null,
      "crossOrigin": null,
      "preload": "auto",
      "defaultPlaybackRate": 1,
      "played": {},
      "seekable": {},
      
      "loop": false,
      "mediaGroup": "",
      "controller": null,
      "controls": false,
      "defaultMuted": false,
      "audioTracks": {
        "0": {}
      },
      "videoTracks": {
        "0": {}
      },
      "textTracks": {},
      "width": 0,
      "height": 0,
      "poster": ""
    };

    this.play = this.play.bind(this);
    this.pause = this.pause.bind(this);
    this.changeCurrentTime = this.changeCurrentTime.bind(this);
    this.seek = this.seek.bind(this);
    this.setMuted = this.setMuted.bind(this);
  }
  componentDidMount() {
    document.body.classList.toggle("lock-page");
    this.refs.player.subscribeToStateChange(this.handleStateChange.bind(this));
  }
  componentWillUnmount() {
    document.body.classList.toggle("lock-page");
  }
  handleStateChange(state, prevState) {
    // copy player state to this component's state
    this.setState({
      player: state,
      currentTime: state.currentTime,
      autoplay:true,
    });
  }

  play() {
    this.refs.player.play();
  }

  pause() {
    this.refs.player.pause();
  }

  changeCurrentTime(seconds) {
    return () => {
      const { player } = this.refs.player.getState();
      const currentTime = player.currentTime;
      this.refs.player.seek(currentTime + seconds);
    };
  }

  seek(seconds) {
    return () => {
      this.refs.player.seek(seconds);
    };
  }

  setMuted(muted) {
    return () => {
      this.refs.player.muted = muted;
    };
  }

  render() {
    return (
      <div className="lock-page">
        <Container>
          <div>

          <Player
          ref="player"
          startTime= "1"
          autoPlay ={true}
        >
              <source src={this.state.source} />
              <ControlBar autoHide={false} disableDefaultControls={true}>
                <CurrentTimeDisplay order={4.1} />
                <TimeDivider order={4.2} />
                <PlaybackRateMenuButton
          rates={[5, 2, 1, 0.5, 0.1]}
          order={7.1}
        />
                <PlayToggle />
              </ControlBar>
            </Player>
            <div className="pb-3">
              <Button onClick={this.changeCurrentTime(10)} className="mr-3">currentTime += 10</Button>
              <Button onClick={this.changeCurrentTime(-10)} className="mr-3">currentTime -= 10</Button>
              <Button onClick={this.seek(50)} className="mr-3">currentTime = 50</Button>
            </div>

          </div>


          {/*  <Col md="6">
            <div className="embed-responsive embed-responsive-16by9">
              <iframe  title="Ads first" className="embed-responsive-item" src="https://www.youtube.com/embed/vlDzYIIOYmM" ></iframe>
            </div>
          </Col> */}
        </Container>
        <div
          className="full-page-background"
          style={{
            backgroundImage: `url(${require("assets/img/bg/dangky.jpg")})`
          }}
        />
      </div>
    );
  }
}

export default LockScreen;
