import React from "react";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  Label,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  Row,
  Col
} from "reactstrap";

const imgIcon = {
  width: '25px'
};

class Register extends React.Component {
  componentDidMount() {
    document.body.classList.toggle("login-page");
  }
  componentWillUnmount() {
    document.body.classList.toggle("login-page");
  }
  constructor(props) {
    super(props);
    this.state = {
      // register form
      registerPhoneState: "",

      // login form
      loginPhone: "",

      loginPhoneState: "",
      // type validation form

      required: "",
      phone: "",

      phoneState: "",

      // range validation form

      range: "",
      min: "",
      max: ""
    };
  }
  // function that verifies if a string has a given length or not
  verifyLength = (value, length) => {
    if (value.length >= length) {
      return true;
    }
    return false;
  };
  // function that verifies if two strings are equal
  compare = (string1, string2) => {
    if (string1 === string2) {
      return true;
    }
    return false;
  };
  // function that verifies if value contains only phone
  verifyPhone = value => {
    var phoneRex = new RegExp("^[0-9]+$");
    if (phoneRex.test(value)) {
      return true;
    }
    return false;
  };
  change = (event, stateName, type, stateNameEqualTo, maxValue) => {
    switch (type) {
      case "phone":
        if (this.verifyPhone(event.target.value)) {
          this.setState({ [stateName + "State"]: "has-success" });
        } else {
          this.setState({ [stateName + "State"]: "has-danger" });
        }
        break;
      default:
        break;
    }
    this.setState({ [stateName]: event.target.value });
  };
  registerClick = () => {
    if (this.state.registerPhoneState === "") {
      this.setState({ registerPhoneState: "has-danger" });
    }
    if (this.state.registerPhoneState === "has-success") {
      this.props.history.push("/auth/login");
    }
  };
  loginClick = () => {
    this.props.history.push("/auth/login");
  };
  render() {
    // taking all the states
    let {
      // register form
      registerPhoneState
    } = this.state;
    return (
      <div className="register-page">
        <Container>
          <Row>
            <Col className="ml-auto col-md-push-6" lg="5" md="5">
              <div className="info-area info-horizontal mt-5">
                <div className="icon icon-primary">
                  <img  alt="Open wifi"  src={require('assets/img/icon/Openwifi-icon.png')} style={imgIcon}/>
                </div>
                <div className="description">
                  <h5 className="info-title">Open WiFi</h5>
                  <p className="description">
                    Open WiFi is revolutionizing the guest WiFi experience
                    for businesses and their customers.
                  </p>
                </div>
              </div>
              <div className="info-area info-horizontal">
                <div className="icon icon-primary">
                  <img  alt="Smart wifi" src={require('assets/img/icon/Smartwifi-icon.png')} style={imgIcon}/>
                  
                </div>
                <div className="description">
                  <h5 className="info-title">Smart WiFi</h5>
                  <p className="description">
                    With Smart WiFi, guests enjoy a simple login experience and
                    venues finally collect a return on their WiFi investment.
                  </p>
                </div>
              </div>
              <div className="info-area info-horizontal">
                <div className="icon icon-primary">
                  
                  <img  alt="Get money" src={require('assets/img/icon/Getmoney-icon.png') } style={imgIcon}/>
                </div>
                <div className="description">
                  <h5 className="info-title">Get money</h5>
                  <p className="description">
                    Improve your guest experience, increase loyalty return visits, and generate additional
                    revenue all from the guest WiFi you give away for free.
                  </p>
                </div>
              </div>
            </Col>
            <Col className="mr-auto col-md-pull-5" lg="4" md="6">
              <Card className="card-signup text-center">
                <CardHeader>
                  <CardTitle tag="h4">Register</CardTitle>
                  <div className="social">
                    <Button className="btn-icon btn-round" color="twitter">
                      <i className="fa fa-twitter" />
                    </Button>
                    <Button className="btn-icon btn-round" color="dribbble">
                      <i className="fa fa-dribbble" />
                    </Button>
                    <Button className="btn-icon btn-round" color="facebook">
                      <i className="fa fa-facebook-f" />
                    </Button>
                    <p className="card-description">or be classical</p>
                  </div>
                </CardHeader>
                <CardBody>
                  <Form action="" className="form" method="">
                    <FormGroup className={`has-label ${registerPhoneState}`}>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="nc-icon nc-mobile" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          placeholder="Mobile phone..."
                          name="phone"
                          type="text"
                          onChange={e =>
                            this.change(e, "registerPhone", "phone")
                          }
                        />
                      </InputGroup>
                      {this.state.registerPhoneState === "has-danger" ? (
                        <label className="error">
                          Please enter a valid Phone Number.
                        </label>
                      ) : null}
                    </FormGroup>
                    <FormGroup check className="text-left">
                      <Label check>
                        <Input defaultChecked type="checkbox" />
                        <span className="form-check-sign" />I agree to the{" "}
                        <a href="#pablo" onClick={e => e.preventDefault()}>
                          terms and conditions
                        </a>
                        .
                      </Label>
                    </FormGroup>
                  </Form>
                </CardBody>
                <CardFooter>
                  <Button
                    className="btn-round mb-3"
                    color="info"
                    href="#"
                    onClick={this.registerClick}
                  >
                    Register
                  </Button>
                  <Button
                    className="btn-round mb-3"
                    color="primary"
                    href="#"
                    onClick={this.loginClick}
                  >
                    Already has Account ?
                  </Button>
                </CardFooter>
              </Card>
            </Col>
          </Row>
        </Container>
        <div
          className="full-page-background"
          style={{
            backgroundImage: `url(${require("assets/img/bg/dangky.jpg")})`
          }}
        />
      </div>
    );
  }
}

export default Register;
