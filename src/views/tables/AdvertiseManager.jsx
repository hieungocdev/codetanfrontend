import React from "react";

// reactstrap components
import {
  Button,
  Card, CardHeader, CardBody, CardTitle,
  Table,
  Row, Col,
  UncontrolledTooltip
} from "reactstrap";

class AdvertiseManager extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      requiredItem: 0,
      brochure: [
        {
          personName: "Andrew Mike",
          company: "Company",
          createdDate: "20/10/2018",
          totalBudget: "€ 99,225",
          status: "Unavailable"
        }, {
          personName: "Bakayarooo Mike",
          company: "HTC Company",
          createdDate: "30/10/2018",
          totalBudget: "€ 99,225",
          status: "Available"
        }
      ]
    }

  }
  deleteItem(modalID) {
    let tempBrochure = this.state.brochure;
    tempBrochure.splice(modalID, 1);
    this.setState({ brochure: tempBrochure });
  }

  hideItem(modalID) {
    let tempbrochure = this.state.brochure;
    if(tempbrochure[modalID].status === 'Available'){
      tempbrochure[modalID].status = 'Unavailable';
    }else {
      tempbrochure[modalID].status = 'Available';
    }
    this.setState({ brochure: tempbrochure });
  }

  render() {
    const brochure = this.state.brochure.map((item, modalID) => {
      return (
          <tr key={modalID}>
            <td className="text-center">{modalID+1}</td>
            <td>
                  {item.personName}
            </td>
            <td>
                  {item.company}
            </td>
            <td className="text-center">
                  {item.createdDate}
            </td>
            <td className="text-center">
                  {item.totalBudget}
            </td>
            <td className="text-right">
                  {item.status}
            </td>
            <td className="text-right">

            <Button
              className="btn-icon"
              color="danger"
              id="tooltip264453216"
              size="sm"
              type="button"
              // onClick={() => this.hideItem(modalID)}
            >
              <i className="fa fa-key" />
            </Button>{" "}
            <UncontrolledTooltip
              delay={0}
              target="tooltip264453216">
              Lock
            </UncontrolledTooltip>


              <Button
                className="btn-icon"
                color="success"
                id="tooltip366246651"
                size="sm"
                type="button"
                data-toggle="modal"
                data-target="#exampleModal"
                //onClick={() => this.replaceModalItem(modalID)}
              >
                <i className="fa fa-edit" />
              </Button>{" "}
              <UncontrolledTooltip
                delay={0}
                target="tooltip366246651"
              >
                Edit
              </UncontrolledTooltip>
              <Button
                className="btn-icon"
                color="danger"
                id="tooltip476609793"
                size="sm"
                type="button"
                onClick={() => this.deleteItem(modalID)}
              >
                <i className="fa fa-times" />
              </Button>{" "}
              <UncontrolledTooltip
                delay={0}
                target="tooltip476609793"
              >
                Delete
              </UncontrolledTooltip>
            </td>
          </tr>

      )
    });
    return (
      <>
        <div className="content">
          <Row>
            <Col md="12">
              <Card>
                <CardHeader>
                  <CardTitle tag="h4">Advertise Manager</CardTitle>
                </CardHeader>
                <CardBody>
                  <Table responsive striped>
                    <thead className="text-primary">
                      <tr>
                        <th className="text-center">ID</th>
                        <th>Name</th>
                        <th className="text-center">Company / Personal</th>
                        <th className="text-center">Created date</th>
                        <th className="text-right">Total budget</th>
                        <th className="text-right">Status</th>
                        <th className="text-right">Function</th>
                      </tr>
                    </thead>
                    <tbody>
                      {brochure}
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

export default AdvertiseManager;
