const getToken =sessionStorage.getItem('accessToken');


const CreateAdmin = (name, email, username, password) => (
    
       fetch('http://139.180.216.19:2018/account/admin/create',
       {   
           method: 'POST',
           headers: {
              'Content-Type': 'application/json',
             Accept: 'application/json',
             'x-gigawatts': '1.21',
             'access-token' : getToken,
            },
           body: JSON.stringify({ name, email, username, password})
       })
       .then(res => res.json())
   );


module.exports = CreateAdmin;
