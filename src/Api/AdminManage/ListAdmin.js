   const ListAdmin = () => {
    const getToken =sessionStorage.getItem('accessToken');
    return fetch('http://139.180.216.19:2018/account/admin/list/?page=1&limit=40',
    {   
        method: 'GET',
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json',
            'access-token' : getToken,
            type: "cors"
         },
        //body: JSON.stringify()
    })
    .then(res => res.json())
    // .then(res => res.json())
    // .then(res => {
    //     let token = localStorage.getItem('accessToken');
    //     console.log("token: ", token);
    // });
 
};

module.exports = ListAdmin;
