const getToken  = () => {
       try {
           const value = sessionStorage.getItem('token');;
           if (value !== null) {
               return value;
           }
           return '';
       } catch (error) {
       // Error retrieving data
           return '';
       }
   };
   
   export default getToken;
   