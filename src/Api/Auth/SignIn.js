const SignIn = (email, password) => (
       fetch('http://localhost:4000/users/signin',
       {   
           method: 'POST',
           headers: {
              'Content-Type': 'application/json',
             Accept: 'application/json',
             'x-gigawatts': '1.21'
            },
           body: JSON.stringify({ email, password })
       })
       .then(res => res.json())
   );
   
   module.exports = SignIn;